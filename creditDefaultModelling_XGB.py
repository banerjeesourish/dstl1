#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan  2 00:15:21 2018

@author: sourish
"""

## Importing necessary libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from xgboost import XGBClassifier
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error, r2_score
#from keras.utils.vis_utils import plot_model
#from keras.utils import plot_model

#Read Dataset
dataset = pd.read_csv('credit_card_details.csv')

#Create variable
X = dataset.iloc[:, 1:24].values # Exclude ID as 1st column and Default payment as last column 
y = dataset.iloc[:, 24].values # select last column as target variable

#TReat categorical data for independent data
from sklearn.preprocessing import LabelEncoder,OneHotEncoder
labelencoder_X = LabelEncoder()
X[:,5] = labelencoder_X.fit_transform(X[:,5])
X[:,6] = labelencoder_X.fit_transform(X[:,6])
X[:,7] = labelencoder_X.fit_transform(X[:,7])
X[:,8] = labelencoder_X.fit_transform(X[:,8])
X[:,9] = labelencoder_X.fit_transform(X[:,9])
X[:,10] = labelencoder_X.fit_transform(X[:,10])
onehotencoder = OneHotEncoder(categorical_features=[2,3]) # Education and Marital Status
X = onehotencoder.fit_transform(X).toarray()
X = np.delete (X , 7, axis=1) # Resolving dummy variable trap for Marital Status
X = X [:,1:] # Resolving dummy variable trap for Education

# Splitting the dataset into the Training set and Test set
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 0)


# Using XGBOOST Classifier
classifier = XGBClassifier(max_depth=5,
                           gamma=0.01,
                           learning_rate=0.01,
                           n_estimators=500)
classifier.fit(X_train, y_train)


# Predicting the Test set results
y_pred = classifier.predict(X_test)

# Making the Confusion Matrix
from sklearn.metrics import confusion_matrix
cm = confusion_matrix(y_test, y_pred)


# Applying k-Fold Cross Validation
from sklearn.model_selection import cross_val_score
accuracies = cross_val_score(estimator = classifier, X = X_train, y = y_train, cv = 10)
accuracies.mean()
accuracies.std()



## Accuracy is ~82% after 100 epoch of training and loss is ~ 0.42

#Making predictions and evaluating the model
y_pred_proba = classifier.predict_proba(X_test)

y_pred_proba = np.delete(y_pred_proba,0,axis=1)

# We are going to use Sorting Smoothing Method to evaluate the model.
# As there are no "Actual" credit default probability present in the data,
# we are going to derive the "Actual" credit default probability using SSM or
# (Sorting Smoothing Method). This method is being used by Cheng Yeh and Che-hui Lien
# in their paper "The comparisons of data mining techniques for the predictive 
# accuracy of probability of default of credit card clients"

# First part of SSM - Sorting the data based on their predicted default probability

# Initializing the array for saving the original indexes
y_pred_ind = np.zeros(len(y_pred_proba))

for i in range(len(y_pred_proba)):
    y_pred_ind[i] = i

# Creating another copy of original target variable and predicted target probability
# appending with their original index    
y_pred_id = np.append(y_pred_ind.reshape(len(y_pred_proba),1), y_pred_proba, axis=1)
y_test_id = np.append(y_pred_ind.reshape(len(y_test),1), y_test.reshape(len(y_test),1), axis=1)

# Sorting of predicted target variable as probability and copying into another array
y_pred_sort = np.asarray(sorted(y_pred_id, key=lambda x : x[1]))

# Second part of SSM - Smoothing the data using Mean Smoothing technique (averaging the data points)

# Initializing the array for getting "Actual" probability
real_proba = np.zeros(len(y_test))

# Number of data points used for smoothing
smoothing_len = 50

# Actual Smoothing applied on the predicted probablity to derive "Actual" probability
for i in range(len(y_pred_sort)):
    true_val =0
    count=0
    for x in range(i-smoothing_len, i+smoothing_len):
        if (x >=0 and x < len(y_pred_sort)):   
            idx = (y_pred_sort[x,0])
            rows = np.where(y_test_id[:,0] == idx)
            true_val = true_val + y_test_id[rows][0,1]
            count = count+1
    real_proba[i] = true_val/(count)        

# Last part of SSM - evaluating the model performance (accuracy of "Actual" vs "Predicted") using Linear Regression
regressor = LinearRegression()
regressor.fit(y_pred_sort[:,1].reshape(-1,1), real_proba.reshape(-1,1))

# Plotting of data and getting the linear regression equation (intercept, coefficient, r-square and mean squared error)
plt.scatter(y_pred_sort[:,1], real_proba, color = 'red')
plt.plot(y_pred_sort[:,1].reshape(-1,1), (regressor.intercept_+regressor.predict(y_pred_sort[:,1].reshape(-1,1))*regressor.coef_) , color = 'blue')
plt.title('Preicted vs Actual Default probability (Test set)')
plt.xlabel('Predicted Credit Score')
plt.ylabel('Actual Credit Score')
plt.show()
print(regressor.intercept_, regressor.coef_, r2_score(real_proba,y_pred_sort[:,1]),mean_squared_error(real_proba,y_pred_sort[:,1]))
# Intercept - -0.016627 (approximately 0)
# Coefficient - 1.0497 (~ 1)
# r square value - 0.966
# mean squared error - 0.001